package dam.androidroberto.u3t2implicitintents;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String IMPLICIT_INTENTS = "ImplicidIntents";
    private EditText etUri, etLocation, etText, etZoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI() {
        etUri = findViewById(R.id.etUri);
        Button btOpenUri = findViewById(R.id.btOpenUri);
        etLocation = findViewById(R.id.etLocation);
        etZoom = findViewById(R.id.etZoom);
        Button btOpenLocation = findViewById(R.id.btOpenLocation);
        etText = findViewById(R.id.etText);
        Button btShare = findViewById(R.id.btShareText);
        Button btMore = findViewById(R.id.btMoreIntents);
        btOpenUri.setOnClickListener(this);
        btOpenLocation.setOnClickListener(this);
        btShare.setOnClickListener(this);
        btMore.setOnClickListener(this);
    }

    private void openWebsite(String urlText) {
        Uri webpage = Uri.parse(urlText);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS, "openWebsite: Can't handle this intent!");
        }
    }

    private void openLocation(String location, String zoom) {
        if (!zoom.isEmpty()) {
            if (Integer.parseInt(zoom) >=1 && Integer.parseInt(zoom) <= 23){
                Uri adressUri = Uri.parse("geo:0,0?q="+ location + "&z=" + zoom);

                Intent intent = new Intent(Intent.ACTION_VIEW, adressUri);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Log.d(IMPLICIT_INTENTS, "openLocation: Can't handle this intent!");
                }
            } else {
                Log.d(IMPLICIT_INTENTS, "openLocation: Invalid Zoom!");
            }
        } else {
            Log.d(IMPLICIT_INTENTS, "openLocation: Empty Zoom!");
        }
    }

    private void shareText(String text) {
        new ShareCompat.IntentBuilder(this).setType("text/plain").setText(text).startChooser();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btOpenUri:
                openWebsite(etUri.getText().toString());
                break;
            case R.id.btOpenLocation:
                openLocation(etLocation.getText().toString(),etZoom.getText().toString());
                break;
            case R.id.btShareText:
                shareText(etText.getText().toString());
                break;
            case R.id.btMoreIntents:
                startActivity(new Intent(this, MoreIntents.class));
                break;
        }
    }
}