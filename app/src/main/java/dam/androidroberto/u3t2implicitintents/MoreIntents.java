package dam.androidroberto.u3t2implicitintents;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MoreIntents extends AppCompatActivity implements View.OnClickListener {

    public static final String IMPLICIT_INTENTS = "ImplicidIntents";
    private EditText etMessage, etTime, etContent, etSeconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_intents);
        setUI();
    }

    private void setUI() {
        etMessage = findViewById(R.id.etMessage);
        etTime = findViewById(R.id.etTime);
        Button btSetAlarm = findViewById(R.id.btSetAlarm);
        etContent = findViewById(R.id.etContent);
        etSeconds = findViewById(R.id.etSeconds);
        Button btStartTimer = findViewById(R.id.btStartTimer);
        Button btBack = findViewById(R.id.btBack);
        Button btShowAlarms = findViewById(R.id.btShowAlarms);
        btShowAlarms.setOnClickListener(this);
        btSetAlarm.setOnClickListener(this);
        btStartTimer.setOnClickListener(this);
        btBack.setOnClickListener(this);
    }


    public void createAlarm(String message, int hour, int minutes) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_HOUR, hour)
                .putExtra(AlarmClock.EXTRA_MINUTES, minutes);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void startTimer(String message, int seconds) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_TIMER)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_LENGTH, seconds);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void showAlarms() {
        Intent intent = new Intent(AlarmClock.ACTION_SHOW_ALARMS);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btSetAlarm:
                createAlarm(etMessage.getText().toString(), Integer.parseInt(etTime.getText().toString().split(":")[0]), Integer.parseInt(etTime.getText().toString().split(":")[1]));
                break;
            case R.id.btStartTimer:
                startTimer(etContent.getText().toString(), Integer.parseInt(etSeconds.getText().toString()));
                break;
            case R.id.btShowAlarms:
                showAlarms();
                break;
            case R.id.btBack:
                startActivity(new Intent(this, MainActivity.class));
                break;
        }
    }
}